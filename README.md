# Home Page

## Name: Gu Haoxuan

## React-CA1 && Agile test

## siteHeader

A parameterized URL included.

![](movies\readme_source\header.png)


## Movie's details

Show the movie's details such as Revenue and stars through the API of TMDB.

![](movies\readme_source\detail.png)


## Cast

The cast can be displayed, but the actor details page has not been produced yet.

![](movies\readme_source\cast.png)



## Pagination

Join pagination, you can turn pages for new movies.

![](movies\readme_source\pagination.png)


## Actors' detail page

Show the actors' details.

![](movies\readme_source\actor_detail.png)